import React from "react"
import { StaticImage } from "gatsby-plugin-image"
import Footer from "../components/footer"

const AboutPage = () => {
	return (
		<div>
			<h1>Sobre min</h1>
		<div><StaticImage src="../images/profile.jpg" /></div>
		<Footer />
		</div>
	)
}

export default AboutPage
